# frozen_string_literal: true

require 'csv'
require_relative 'quivaou'

module Hapardi
  class QuivaouFromCsv
    def initialize(filepath, shortest: true, stack_limit: nil, prune_ratio: nil)
      @filepath = filepath
      @stack_limit = stack_limit
      @prune_ratio = prune_ratio
      @shortest = !!shortest
    end

    def call
      Hapardi::Quivaou.new(stack_limit:, prune_ratio:).call(matrix: coerced_matrix, shortest:)
    end

    def coerced_matrix
      matrix.map do |row|
        row.map do |cell|
          case cell
          when 'OK' then Hapardi::Quivaou::OK
          when 'NC' then Hapardi::Quivaou::NC
          when 'ZZ' then Hapardi::Quivaou::ZZ
          when 'NO' then Hapardi::Quivaou::NO
          when 'KO' then Hapardi::Quivaou::KO
          when 1..9 then cell
          when '1'..'9' then cell.to_i
          else
            raise Hapardi::MatrixError, "Invalid value '#{cell}': Cell value must be one of KO, OK, ZZ, NC or a number between 1 & 9"
          end
        end
      end
    end

    private

    attr_reader :filepath, :shortest, :stack_limit, :prune_ratio

    def csv
      @csv ||= CSV.read(filepath, headers: true)
    end

    def matrix
      @matrix ||= csv.map { |row| row[5...] }
    end

    def names
      @names ||= csv.map { |row| row[...5] }
    end
  end
end
