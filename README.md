<!-- PROJECT SHIELDS -->
[![Contributors][contributors-shield]][contributors-url]
[![Issues][issues-shield]][issues-url]
[![WTFPL License][license-shield]][license-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/akarizm/quivaou">
    <img src="images/logo.svg" alt="Logo" width="307" height="142">
  </a>

  <h3 align="center">Quivaoù?</h3>

  <p align="center">
    Trouver toiture à son souhait.
    <br />
    <a href="https://gitlab.com/akarizm/quivaou"><strong>Documentation »</strong></a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
## Table des Matières

* [À Propos du Projet](#à-propos-du-projet)
  * [Construit Avec](#construit-avec)
* [Pour Commencer](#pour-commencer)
  * [Installation](#installation)
* [Usage](#usage)
  * [Pondération de la Matrice](#pondération-de-la-matrice)
* [Licence](#licence)
* [Contact](#contact)
* [Remerciements](#remerciements)

## À Propos du Projet

Il s'agit avant tout d'un « projet jouet » pour découvrir les outils de parcours
de graphe en Ruby.

Le but est de trouver la meilleure affectation possible d'un logement à un foyer
sur la base des désidératas de chaque foyer. Pour cela, on va parcourir la
copropriété à l'aide d'un algorithme de Dijkstra.

### Construit Avec

* [Ruby 3.1](https://www.ruby-lang.org/fr/downloads/)
* [RGL](https://www.rubydoc.info/github/monora/rgl)

## Pour commencer

Pour disposer d'une copie locale, suivez ces quelques étapes simples.

### Prérequis

Vous aurez besoin de [Bundler] pour installer les dépendances Ruby.

[Bundler]: https://bundler.io/

### Installation

1. Cloner le dépôt

```sh
git clone https://gitlab.com/akarizm/quivaou.git
```

2. Installer Quivaoù?

```sh
cd quivaou && bundle install
```

## Usage

```
bundle exec irb
```

Puis dans la console IRB

```irb
irb(main):001:0> require_relative 'quivaou_from_csv'
irb(main):002:0> qcsv = Hapardi::QuivaouFromCsv.new("quivaou.csv")
irb(main):003:0> qcsv.call
[MATRX] use a 25x25 matrix…
[INOUT] add source & target edges…
[GRAPH] add non KO edges…
=> ["IN", "FAHG", "FBHD", "FCHO", "FDHI", "FEHR", "FFHX", "FGHI", "FHHK", "FIHP", "FJHF", "FKHU", "FHHY", "FMHM", "FNHV", "FOHG", "FPHA", "FQHY", "FRHM", "FSHN", "FTHB", "FUHC", "FVHB", "FWHW", "FXHD", "FYHX", "OUT"]
```

Il est possible de passer des arguments à l'instanciation de la classe
`Hapardi::QuivaouFromCsv` :

- `shortest` (défaut: `true`) pour trouver le plus court chemin ; mettre à `false` pour trouver tous les chemins possibles
- `stack_limit` (défaut: 100 000) limite à partir de laquelle réaliser l'élaguage
- `prune` (défault: 0.9) ratio à conserver lors de l'élaguage.

Il est également possible de ne pas passer par un fichier CSV en appelant
directement `Hapardi::Quivaou` avec la matrice 3x3 d'exemple :

```ruby
@sample_matrix ||= [
  # FA FB FC
  [ZZ, 1, KO], # HA
  [ZZ, 2, KO], # HB
  [KO, KO, 1]  # HC
]
```

```irb
irb(main):001:0> require_relative 'quivaou'
irb(main):002:0> qcsv = Hapardi::Quivaou.new.call
[MATRX] use a 3x3 matrix…
[INOUT] add source & target edges…
[GRAPH] add non KO edges…
=> ["IN", "FAHB", "FBHA", "FCHC", "OUT"]
```

On peut aussi demander à obtenir tous les chemins possibles :

```irb
irb(main):001:0> require_relative 'quivaou'
irb(main):002:0> qcsv = Hapardi::Quivaou.new.call(shortest: false)
[MATRX] use a 3x3 matrix…
[INOUT] add source & target edges…
[GRAPH] add non KO edges…
[ROUTE] search routes from source IN…
[ROUTE] examine edge (IN, FAHA)
[ROUTE] examine edge (IN, FAHB)
[ROUTE] examine edge (FAHA, FBHB)
[STATS] 2 routes so far
[ROUTE] filter one home, one family…
[STATS] 2 routes filtered
[ROUTE] prune 9th decile…
[STATS] 2 routes filtered
[ROUTE] examine edge (FAHB, FBHA)
[STATS] 2 routes so far
[ROUTE] filter one home, one family…
[STATS] 2 routes filtered
[ROUTE] prune 9th decile…
[STATS] 2 routes filtered
[ROUTE] examine edge (FBHB, FCHC)
[STATS] 2 routes so far
[ROUTE] filter one home, one family…
[STATS] 2 routes filtered
[ROUTE] prune 9th decile…
[STATS] 2 routes filtered
[ROUTE] examine edge (FBHA, FCHC)
[STATS] 2 routes so far
[ROUTE] filter one home, one family…
[STATS] 2 routes filtered
[ROUTE] prune 9th decile…
[STATS] 2 routes filtered
[ROUTE] examine edge (FCHC, OUT)
[STATS] 2 routes so far
[ROUTE] filter one home, one family…
[STATS] 2 routes filtered
[ROUTE] prune 9th decile…
[STATS] 2 routes filtered
[STATS] 2 routes found
[ROUTE] filter on routes reaching target OUT…
[STATS] 2 routes reaching target
[ROUTE] filter only accepted ones…
[STATS] 2 routes accepted
[ROUTE] filter one family, one home…
[STATS] 2 routes filtered
[FINAL] printing result…
distance = 15 ; steps = {IN,FAHB,FBHA,FCHC,OUT}
distance = 16 ; steps = {IN,FAHA,FBHB,FCHC,OUT}
```

### Pondération de la Matrice

Les pondérations inscrites dans la matrice peuvent être des valeurs numériques
— de préférence de 1 à 4 ; 1 étant le premier choix — ou l'une des valeurs
suivantes :

| valeur | poids | description |
|:-:|:-:|:---|
| OK | 1 | logement accepté |
| NC | 8 | à discuter |
| ZZ | 13 | ne se prononce pas |
| NO | 21 | logement refusé |
| KO | ∞ | logement incompatible |

## Licence

Distribué sous licence WTFPL version 2. Consultez le fichier `LICENSE` pour plus
d'informations.

## Contact

François Vantomme - [@akarzim](https://mastodon.host/@akarzim)

Lien du projet : <https://gitlab.com/akarizm/quivaou>

## Remerciements

* [Othneil Drew][othneildrew] pour [ce modèle de readme][readme-template]

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/gitlab/contributors/akarzim/quivaou.svg?style=flat-square
[contributors-url]: https://gitlab.com/akarzim/quivaou/-/graphs/main
[issues-shield]: https://img.shields.io/gitlab/issues/akarzim/quivaou.svg?style=flat-square
[issues-url]: https://gitlab.com/akarzim/quivaou/-/issues
[license-shield]: https://img.shields.io/gitlab/license/akarzim/quivaou.svg?style=flat-square
[license-url]: https://gitlab.com/akarzim/quivaou/blob/master/LICENSE.txt
[othneildrew]: https://gitlab.com/othneildrew
[readme-template]: https://gitlab.com/othneildrew/Best-README-Template
