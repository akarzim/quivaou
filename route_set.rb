# frozen_string_literal: true

require_relative 'route'

module Hapardi
  class RouteSet
    DEFAULT_STACK_LIMIT = 100_000
    DEFAULT_PRUNE_RATIO = 0.9

    def initialize(source:, edge_weights: nil, stack_limit: nil, prune_ratio: nil)
      @routes = []
      @source = source
      @edge_weights = Hash(edge_weights)
      @stack_limit = stack_limit || DEFAULT_STACK_LIMIT
      @prune_ratio = prune_ratio || DEFAULT_PRUNE_RATIO
    end

    def count
      routes.count
    end

    def add_edge(from, to, step_regex:)
      w = edge_weights[[from, to]]
      return add_initial_edge(from, to, weight: w) if from == source

      self.routes = routes.flat_map do |route|
        if route.ends_with?(from)
          route.add_edge(from, to, weight: w)
        elsif route.include?(from)
          route.fork_with_edge(from, to, weight: w)
        else
          route
        end
      end

      puts "[STATS] #{count} routes so far"
      puts '[ROUTE] filter one home, one family…'
      one_home_one_family!(step_regex:)
      puts "[STATS] #{count} routes filtered"
      puts "[ROUTE] prune #{(prune_ratio * 10).to_i}th decile…"
      prune!
      puts "[STATS] #{count} routes filtered"
    end

    def pretty_print
      routes.sort.map(&:pretty_print)
    end

    def reaching!(target)
      routes.select! { |route| route.ends_with?(target) }
    end

    def one_family_one_home!(step_regex:)
      routes.select! do |route|
        families = route.steps.map do |step|
          step_regex.match?(step) ? step_regex.match(step)['family'] : step
        end

        families.uniq.length == route.length
      end
    end

    def one_home_one_family!(step_regex:)
      routes.select! do |route|
        homes = route.steps.map do |step|
          step_regex.match?(step) ? step_regex.match(step)['home'] : step
        end

        homes.uniq.length == route.length
      end
    end

    def accepted_ones!
      routes.select! { |route| route.size < Hapardi::Quivaou::NO }
    end

    def prune!
      return routes if count < stack_limit

      routes.sort!
      puts "[STATS] routes size between #{routes.minmax.map(&:size)}"
      idx = (prune_ratio * routes.count).to_i
      self.routes = routes[...idx]
    end

    private

    attr_accessor :routes
    attr_reader :source, :edge_weights, :stack_limit, :prune_ratio

    def add_initial_edge(from, to, weight: 0)
      route = Hapardi::Route.new
      route.init(from, to, weight:)
      routes << route
      routes
    end
  end
end
