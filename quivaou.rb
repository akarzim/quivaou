# frozen_string_literal: true

require 'rgl/adjacency'
require 'rgl/dijkstra'
require 'rgl/traversal'
require_relative 'route_set'

module Hapardi
  MatrixError = Class.new(StandardError)

  # Le but de cette classe est de trouver la meilleure affectation possible d'un logement (HA..HY) à un foyer
  # (FA..FY) sur la base des désidératas de chaque foyer. Pour cela, on va parcourir la copropriété à l'aide
  # d'un algorithme de Dijkstra, en allant de logement en logement, de l'entrée (IN) vers la sortie (OUT).
  #
  # Le poids appliqué à chaque bout de chemin (vecteur) allant d'un logement à un autre sera stipulé comme
  # ceci dans la matrice de décision :
  # 1 = choix de prédilection
  # 2 = choix secondaire
  # n = n-ième choix
  # NC = choix négociable (càd OK si on ne peut pas faire autrement)
  # ZZ = logement compatible, ne se prononce pas
  # NO = refus du logement
  # KO = choix inacceptable
  #
  # @exemple voici une matrice de décision sur la base de 3 foyers & 3 logements :
  # matrix = [
  # #  FA  FB  FC
  #   [ZZ,  1, KO], # HA
  #   [ZZ,  2, KO], # HB
  #   [KO, KO,  1]  # HC
  # ]
  #
  # Cela donnera le résultat suivant : ["IN", "FBHA", "FAHB", "FCHC", "OUT"]
  class Quivaou
    # La pesée
    OK = 1
    NC = 8
    ZZ = 13
    NO = 21
    KO = Float::INFINITY

    SOURCE = 'IN'
    TARGET = 'OUT'
    VERTICE_REGEX = /(?<family>\w{2})(?<home>\w{2})/

    def initialize(stack_limit: nil, prune_ratio: nil)
      @graph = RGL::DirectedAdjacencyGraph.new
      @edge_weights = {}
      @stack_limit = stack_limit
      @prune_ratio = prune_ratio
    end

    def call(matrix: nil, shortest: true)
      matrix ||= sample_matrix

      raise Hapardi::MatrixError, 'must be a square matrix' if matrix.size != matrix[0].size
      raise Hapardi::MatrixError, 'please fill every matrix sell' if matrix.map(&:size).minmax.uniq.size != 1

      puts "[MATRX] use a #{matrix[0].size}x#{matrix.size} matrix…"

      # On commence par créer un vecteur pour chaque foyer :
      # - en entrée vers le premier logement
      # - en sortie depuis le dernier logement
      puts '[INOUT] add source & target edges…'
      inout(matrix)

      # Ensuite on parcours la matrice par paquet de 2 lignes (logements) consécutives ; et pour chaque couple
      # foyer/logement, on crée un vecteur vers les couples de la seconde ligne si le poids n'est pas KO.
      puts '[GRAPH] add non KO edges…'
      add_edges(matrix)

      # Pour finalement rechercher le chemin le plus court de l'entrée vers la sortie…
      return graph.dijkstra_shortest_path(edge_weights, SOURCE, TARGET) if shortest

      # …ou bien tous les chemins de l'entrée vers la sortie, triés par coût
      puts "[ROUTE] search routes from source #{SOURCE}…"
      routes = bfs_search_routes_from(SOURCE, edge_weights, stack_limit, prune_ratio)
      puts "[STATS] #{routes.count} routes found"
      puts "[ROUTE] filter on routes reaching target #{TARGET}…"
      routes.reaching!(TARGET)
      puts "[STATS] #{routes.count} routes reaching target"
      puts '[ROUTE] filter only accepted ones…'
      routes.accepted_ones!
      puts "[STATS] #{routes.count} routes accepted"
      puts '[ROUTE] filter one family, one home…'
      routes.one_family_one_home!(step_regex: VERTICE_REGEX)
      puts "[STATS] #{routes.count} routes filtered"
      puts '[FINAL] printing result…'
      puts routes.pretty_print
    end

    private

    attr_reader :graph, :edge_weights, :stack_limit, :prune_ratio

    def bfs_search_routes_from(source, edge_weights, stack_limit, prune_ratio)
      bfs = graph.bfs_iterator(source)
      routes = RouteSet.new(source:, edge_weights:, stack_limit:, prune_ratio:)

      bfs.set_examine_edge_event_handler do |from, to|
        puts "[ROUTE] examine edge (#{from}, #{to})"
        routes.add_edge(from, to, step_regex: VERTICE_REGEX)
      end

      bfs.set_to_end # does the search
      routes
    end

    def inout(matrix)
      size = matrix.size
      first_family, last_family = families(size).minmax

      homes(size).each_with_index do |home, lidx|
        flog_in = matrix[0][lidx]
        edge_weights[[SOURCE, "#{first_family}#{home}"]] = flog_in unless flog_in == KO
        edge_weights[["#{last_family}#{home}", TARGET]] = 0
      end
    end

    def add_edges(matrix)
      size = matrix.size
      matrix.each_cons(2).with_index do |(flogs_in, flogs_out), fidx|
        flogs_in.product(flogs_out).each_with_index do |(flog_in, flog_out), lidx|
          next if flog_in == KO || flog_out == KO

          qlidx, mlidx = lidx.divmod(size)
          next if qlidx == mlidx

          flin = refmat(size)[fidx][qlidx]
          flout = refmat(size)[fidx + 1][mlidx]

          edge_weights[[flin, flout]] = flog_out
        end
      end

      edge_weights.each_key { |flog_in, flog_out| graph.add_edge(flog_in, flog_out) }
    end

    def families_enum
      @families_enum ||= Enumerator.produce('FA', &:succ)
    end

    def families(size)
      @families ||= {}
      @families[size] ||= families_enum.rewind.first(size)
    end

    def homes_enum
      @homes_enum ||= Enumerator.produce('HA', &:succ)
    end

    def homes(size)
      @homes ||= {}
      @homes[size] ||= homes_enum.rewind.first(size)
    end

    # @example refmat = [
    #   ["FAHA", "FBHA", "FCHA"],
    #   ["FAHB", "FBHB", "FCHB"],
    #   ["FAHC", "FBHC", "FCHC"]
    # ]
    def refmat(size)
      @refmat ||= {}
      @refmat[size] ||= Array.new(size) { Array.new(size) }.tap do |ary|
        homes(size).each_with_index do |home, lidx|
          families(size).each_with_index do |family, fidx|
            ary[fidx][lidx] = "#{family}#{home}"
          end
        end
      end
    end

    def sample_matrix
      @sample_matrix ||= [
        # FA FB FC
        [ZZ, 1, KO], # HA
        [ZZ, 2, KO], # HB
        [KO, KO, 1]  # HC
      ]
    end
  end
end
