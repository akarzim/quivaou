# frozen_string_literal: true

module Hapardi
  RouteError = Class.new(StandardError)

  class Route
    include Comparable

    def initialize(distance_combinator: nil)
      @distance_combinator = distance_combinator
      @distance = []
      @steps = []
    end

    attr_reader :steps

    def <=>(other)
      size <=> other.size
    end

    def size
      distance.sum
    end

    def length
      steps.size
    end

    def ends_with?(step)
      steps.last == step
    end

    def include?(step)
      steps.include?(step)
    end

    def init(from, to, weight: 0)
      raise Hapardi::RouteError, "route already initialized with steps #{steps.join(',')}" if steps.any?

      self.steps = [from, to]
      self.distance = distance_combinator.call(distance, weight)
      self
    end

    def add_edge(from, to, weight: 0)
      return self unless ends_with?(from)

      steps << to
      self.distance = distance_combinator.call(distance, weight)
      self
    end

    def fork_with_edge(from, to, weight: 0)
      puts "[ROUTE] fork route {#{pretty_print}} with edge (#{from}, #{to})…"
      origin = dup
      fork_route = truncate_after(from).add_edge(from, to, weight:)
      [origin, fork_route]
    end

    def pretty_print
      "distance = #{size} ; steps = {#{steps.join(',')}}"
    end

    private

    attr_accessor :distance
    attr_writer :steps

    def truncate_after(step)
      self.steps = steps.take_while { |s| s != step } << step
      self.distance = distance[...steps.size - 1]
      self
    end

    def distance_combinator
      @distance_combinator || default_distance_combinator
    end

    def default_distance_combinator
      ->(distance, weight) { distance << weight }
    end
  end
end
